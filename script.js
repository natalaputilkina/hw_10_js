/* 

Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?

Створення:
- document.createElement(tag) - Створює новий вузол елемента з
вказаним тегом;
- document.createTextNode(text) - Створює новий вузол тексту із заданим
текстом.

Додавання:
elem.insertAdjacentHTML(where, html).

Перший параметр - це кодове слово, яке вказує, де вставити відносно
elem. Має бути одним із наступних:
- "beforebegin" – вставити html безпосередньо перед elem,
- "afterbegin" – вставити html в elem, на початку,
- "beforeend" – вставити html в elem, в кінці,
- "afterend" – вставити html безпосередньо після elem.
Другий параметр - це рядок HTML, який вставляється


2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

для видалення елементу необхідно його знайти, наприклад:

const navigation = document.querySelector('.navigation')

потім видалити:

navigation.remove()


3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

- node.append(...nodes або strings) – додає вузли або рядки в кінець вузла,;
- node.prepend(...nodes або strings) – вставляє вузли або рядки в початок
вузла;
- node.before(...nodes або strings) – вставляє вузли або рядки перед
вузлом;
- node.after(...nodes або strings) – вставляє вузли або рядки після вузла;
- node.replaceWith(...nodes або strings) – замінює вузол заданими вузлами
або рядками.
 */

// Практичні завдання
//  1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.

const footer = document.querySelector('footer p')

const newElement = document.createElement('a');
newElement.getAttribute('href');
const newElementLink = '#'
newElement.textContent = 'Learn More';
newElement.setAttribute('href', newElementLink)

footer.after(newElement)
 
//  2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
//  Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
//  Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
//  Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
//  Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

const mainSelect = document.querySelector('main .features')
console.log(mainSelect)

const newSelect = document.createElement('select');
console.log(newSelect);

mainSelect.before(newSelect)

const fragmentOption = document.createDocumentFragment()

for(let i = 4; i >= 1; i--){
    const newOption = document.createElement('option')
    newOption.getAttribute('value')
    const newOptionValue = `${i}`
    newOption.textContent = `${i} Stars`
    newOption.setAttribute('value', newOptionValue)
    fragmentOption.append(newOption)
    
}

newSelect.append(fragmentOption)



 